//
//  FilterViewController.h

//
//  Created by Rahul Sharma on 31/12/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SubCategory.h"
/*Ajay Thakur*/

@protocol filterDelegate <NSObject>

-(void)applyfilterForSelectedLists;

@end
@interface FilterViewController : UIViewController <UITableViewDelegate,UITableViewDataSource ,UIGestureRecognizerDelegate , UITextFieldDelegate>
{
    NSString *minPrce,*maxPrce, *selectedLocation;
    double lattitude,longitude;
    int distnce;
    
}

@property (weak, nonatomic) IBOutlet UIView *viewForTableView;
@property (strong, nonatomic) IBOutlet UIButton *navCancelButtonOutlet;
@property (strong, nonatomic) IBOutlet UIButton *navResetButtonOutlet;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIButton *applyButtonOutlet;

- (IBAction)applyFiltersButtonAction:(id)sender;

@property BOOL checkArrayOfFilters;
@property(nonatomic,weak) NSArray *categoryArray;
@property(nonatomic,weak)id<filterDelegate>delegate;

@property(nonatomic,strong)FilterListings *filterListings;

@end

