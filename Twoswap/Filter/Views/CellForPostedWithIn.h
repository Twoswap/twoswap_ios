//
//  CellForPostedWithIn.h

//
//  Created by Rahul Sharma on 03/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellForPostedWithIn : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelForPostIn;
@property (weak, nonatomic) IBOutlet UIImageView *imageMark;


@end
