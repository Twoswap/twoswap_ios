//
//  ReportPostTableViewCell.h

//
//  Created by Rahul_Sharma on 02/05/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportPostTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *reportTypeLabel;

@property (strong, nonatomic) IBOutlet UITextView *textview;
@property (strong, nonatomic) IBOutlet UIImageView *markImageView;

-(void)setPropertiesForObjectForIndexPath :(NSIndexPath *)indexPath forSelected:(NSInteger)selectedIndex;

@property (strong, nonatomic) IBOutlet UIView *viewForAdditionalNote;

@end
