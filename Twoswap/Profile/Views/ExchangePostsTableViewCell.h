//
//  ExchangePostsTableViewCell.h
//  Tac Traderz
//
//  Created by 3Embed on 31/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Exchanged.h"

@interface ExchangePostsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *postImage;

@property (weak, nonatomic) IBOutlet UIImageView *userPostImage;

@property (weak, nonatomic) IBOutlet UIImageView *exchangedPostImage;
@property (weak, nonatomic) IBOutlet UILabel *exchangedDescription;

-(void)setExchangedPostsFor :(Exchanged *)exchanged;

@property (weak, nonatomic) IBOutlet UILabel *timeStampLabel;


@end
