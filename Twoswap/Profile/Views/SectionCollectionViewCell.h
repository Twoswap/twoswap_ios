//
//  SectionCollectionViewCell.h
//  Tac Traderz
//
//  Created by 3Embed on 16/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RFQuiltLayout.h"
#import "ProfileViewController.h"

@interface SectionCollectionViewCell : UICollectionViewCell<UICollectionViewDataSource, UICollectionViewDelegate, RFQuiltLayoutDelegate, UITableViewDataSource, UITableViewDelegate>

@property (nonatomic,strong)NSArray *postsArray;
@property (nonatomic,strong)ProfileViewController *profileVC;
@property (weak, nonatomic) IBOutlet UICollectionView *postsCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *soldPostCollectionView;

@property (weak, nonatomic) IBOutlet UICollectionView *likesCollectionView;

@property (weak, nonatomic) IBOutlet UITableView *exchangesTableView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *postCollectionViewHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *soldCollectionHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *likesCollectionHeight;


@end
