//
//  SectionCollectionViewCell.m
//  Tac Traderz
//
//  Created by 3Embed on 16/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "SectionCollectionViewCell.h"
#import "PostsCollectionViewCell.h"
#import "UserDetailsTableViewCell.h"

@implementation SectionCollectionViewCell

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    
    [self RFQuiltLayoutIntialization];
}

-(void)prepareForReuse
{
    [super prepareForReuse];
}

#pragma mark - RFQuiltLayout Intialization

/**
 This method will create an object for RFQuiltLayout.
 */
-(void)RFQuiltLayoutIntialization
{
    RFQuiltLayout* layout = [[RFQuiltLayout alloc]init];
    layout.direction = UICollectionViewScrollDirectionVertical;
    if(self.profileVC.view.frame.size.width == 375)
    {
        layout.blockPixels = CGSizeMake( 37,31);
    }
    else if(self.profileVC.view.frame.size.width == 414)
    {
        layout.blockPixels = CGSizeMake( 102,31);
    }
    
    else
    {
        layout.blockPixels = CGSizeMake( 79,31);
    }
    self.postsCollectionView.collectionViewLayout = layout;
    layout.delegate=self;
    
    RFQuiltLayout* soldLayout = [[RFQuiltLayout alloc]init];
    soldLayout.direction = UICollectionViewScrollDirectionVertical;
    if(self.profileVC.view.frame.size.width == 375)
        soldLayout.blockPixels = CGSizeMake( 37,31);
    else if(self.profileVC.view.frame.size.width == 414)
    {
        soldLayout.blockPixels = CGSizeMake( 102,31);
    }
    
    else
        soldLayout.blockPixels = CGSizeMake( 79,31);
    self.soldPostCollectionView.collectionViewLayout = soldLayout;
    soldLayout.delegate=self;
    
    RFQuiltLayout* likesLayout = [[RFQuiltLayout alloc]init];
    likesLayout.direction = UICollectionViewScrollDirectionVertical;
    if(self.profileVC.view.frame.size.width == 375)
        likesLayout.blockPixels = CGSizeMake( 37,31);
    else if(self.profileVC.view.frame.size.width == 414)
    {
        likesLayout.blockPixels = CGSizeMake( 102,31);
    }
    
    else
        layout.blockPixels = CGSizeMake( 79,31);
    self.likesCollectionView.collectionViewLayout = likesLayout;
    likesLayout.delegate=self;
    
    
}

/*------------------------------*/
#pragma mark- CollectionView DataSource Method
/*------------------------------*/

-(NSInteger )collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.postsArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    PostsCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"postCollectionViewCell" forIndexPath:indexPath];
    [cell setProducts:self.postsArray[indexPath.row]];
    return cell;
}



#pragma mark –
#pragma mark – RFQuiltLayoutDelegate

-(CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout blockSizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.postsArray.count && indexPath.row < self.postsArray.count){
    return [Helper blockSizeOfProduct:self.postsArray[indexPath.row] view:self.profileVC.view forHome:YES];
    }

    return CGSizeZero;
}


- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetsForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return UIEdgeInsetsMake (5,5,0,0);
}




#pragma mark - TableView Delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 0;
}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserDetailsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserDetailsTableViewCell"];
    return cell;
}





@end
