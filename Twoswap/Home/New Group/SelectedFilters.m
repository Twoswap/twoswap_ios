//
//  SelectedFilters.m
//  CollegeStax
//
//  Created by 3Embed on 21/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "SelectedFilters.h"

@implementation SelectedFilters


-(instancetype)initWithDictionary:(NSDictionary *)response
{
    self = [super init];
    if (!self) { return nil; }
    
    self.name = flStrForObj(response[@"value"]);
    self.type = flStrForObj(response[@"typeOfFilter"]);
    self.image = flStrForObj(response[@"image"]);
    return self;
}


+(NSMutableArray *) arrayOfSeletedFilters :(NSArray *)responseData
{
    NSMutableArray *selectedFilterArray = [[NSMutableArray alloc]init];
    for(NSDictionary *filterDic in responseData) {
        
        SelectedFilters *filters = [[SelectedFilters alloc]initWithDictionary:filterDic] ;
        [selectedFilterArray addObject:filters];
    }
    
    return selectedFilterArray ;
}


@end
