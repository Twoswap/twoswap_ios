//
//  Category.m
//  CollegeStax
//
//  Created by Rahul Sharma on 18/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "Category.h"
#import "Filter.h"

@implementation Category

-(instancetype)initWithDictionary:(NSDictionary *)response
{
    self = [super init];
    if (!self) { return nil; }
    
    self.name = flStrForObj(response[@"name"]);
    self.activeimage = flStrForObj(response[@"activeimage"]);
    self.filterCount = [flStrForObj(response[@"filterCount"])integerValue];
    self.subCategoryCount = [flStrForObj(response[@"subCategoryCount"])integerValue];
    self.filterArray = [NSMutableArray new];
    self.filterArray = [Filter arrayOfFilters:response[@"filter"]];
    return self;
}


+(NSMutableArray *) arrayOfCategory :(NSArray *)responseData
{
    NSMutableArray *categoryArray = [[NSMutableArray alloc]init];
    for(NSDictionary *categoryDic in responseData) {
        
        Category *category = [[Category alloc]initWithDictionary:categoryDic] ;
        [categoryArray addObject:category];
    }
    
    return categoryArray ;
}
@end



