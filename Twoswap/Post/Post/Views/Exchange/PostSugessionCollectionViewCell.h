//
//  PostSugessionCollectionViewCell.h
//  Tac Traderz
//
//  Created by 3Embed on 06/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PostSugessionCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *postsuggessionName;

@end
