//
//  SwapListCollectionViewCell.h
//  Tac Traderz
//
//  Created by 3Embed on 04/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SwapListCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (weak, nonatomic) IBOutlet UIButton *removeButtonOutlet;

@property (weak, nonatomic) IBOutlet UILabel *swapProductName;

-(void)setSwapPostsWithdata:(NSArray *)swapPostArray forIndexPath:(NSIndexPath *)indexPath;

-(void)addMoreSwapPostsButton :(NSIndexPath *)indexPath;
@end
