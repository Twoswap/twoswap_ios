//
//  InformationTableViewCell.m
//  CollegeStax
//
//  Created by 3Embed on 12/06/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "InformationTableViewCell.h"
#import "PropertiesCollectionViewCell.h"

@implementation InformationTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



/*--------------------------------------*/
#pragma mark -
#pragma mark - CollectionView DataSource Method
/*--------------------------------------*/

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    
    return self.product.propertyArray.count ;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PropertiesCollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"PropertiesCell" forIndexPath:indexPath];
    
    if(indexPath.row % 2 == 0)
    {
        [cell SetLeftProductProperties:self.product.propertyArray[indexPath.row]];
    }
    else
    {
        [cell SetRightProductProperties:self.product.propertyArray[indexPath.row]];
    }
    
    return cell;
}

/*-------------------------------------------*/
#pragma mark -
#pragma mark - CollectionView Delegate Method
/*-------------------------------------------*/


-(CGSize)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    return CGSizeMake((collectionView.frame.size.width/2),50);
}

@end
