//
//  CellForSorting.h

//
//  Created by Rahul Sharma on 03/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellForSorting : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelForSortBy;
@property (weak, nonatomic) IBOutlet UIImageView *imageMark;

@end
