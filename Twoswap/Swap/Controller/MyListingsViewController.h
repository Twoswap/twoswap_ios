//
//  MyListingsViewController.h
//  Tac Traderz
//
//  Created by 3Embed on 09/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyListingsViewController : UIViewController


@property (strong, nonatomic) ProductDetails *product;
@property (weak, nonatomic) IBOutlet UIButton *swapButtonOutlet;
- (IBAction)swapButtonAction:(id)sender;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewOutlet;
- (IBAction)backButtonAction:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *emptyBackgroundView;

@end
