//
//  RTL.h
//  Picogram
//
//  Created by Rahul Sharma on 06/11/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RTL : NSObject


@property (nonatomic) bool isRTL ;

-(void)initWithData:(BOOL) value ;
+ (id)sharedInstance;
@end
