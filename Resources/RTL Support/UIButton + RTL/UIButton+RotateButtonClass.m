//
//  UIButton+RotateButtonClass.m
//  Jaiecom
//
//  Created by Rahulsharma on 28/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "UIButton+RotateButtonClass.h"

@implementation UIButton (RotateButtonClass)


-(void)rotateButton {

    if([[RTL sharedInstance] isRTL]){
    self.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    }

}

-(void)buttonTextAlignMent
{
    if([[RTL sharedInstance] isRTL]){
    self.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    }
}

-(void)buttonContentInsets:(CGFloat) spacing
{
    if([[RTL sharedInstance] isRTL]){
        //self.contentEdgeInsets = UIEdgeInsetsMake(0, 20,0, 15);
    }
    else
    {
        // self.contentEdgeInsets = UIEdgeInsetsMake(0, 20,0, 15);
    }
}

-(void)buttonImageInsets:(CGFloat) spacing
{
    if([[RTL sharedInstance] isRTL]){
        self.imageEdgeInsets = UIEdgeInsetsMake(0, 0,0, -spacing);
    }
    else
    {
        self.imageEdgeInsets = UIEdgeInsetsMake(0, -spacing,0, 0);
    }
}




@end
