//
//  UIButton+RotateButtonClass.h
//  Jaiecom
//
//  Created by Rahulsharma on 28/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (RotateButtonClass)

-(void)rotateButton;

-(void)buttonTextAlignMent;

-(void)buttonContentInsets:(CGFloat) spacing ;

-(void)buttonImageInsets:(CGFloat) spacing ;
@end
