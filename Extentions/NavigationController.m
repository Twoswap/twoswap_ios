//
//  NavigationController.m
//  Orzdy
//
//  Created by Rahul Sharma on 19/02/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "NavigationController.h"

@interface NavigationController ()<UINavigationControllerDelegate, UIGestureRecognizerDelegate>
@end

@implementation NavigationController
{
    BOOL isPushingViewController;
    id savedGestureRecognizerDelegate;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    self.delegate = self ;
    self.interactivePopGestureRecognizer.delegate = self ;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    self.interactivePopGestureRecognizer.enabled = YES;
    [super pushViewController:viewController animated:animated];
}


-(BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    if (self.viewControllers.count > 1)
      {
        return YES;
      }
            else
      {
          self.interactivePopGestureRecognizer.enabled = NO;
          return NO;
      }

}


@end
